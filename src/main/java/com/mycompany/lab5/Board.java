/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

/**
 *
 * @author informatics
 */
public class Board {
    private char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    private Player currentPlayer;
    private Player player1;
    private Player player2;
    private int row, col;
    
    public Board(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;

    }
    
    public char[][] getBoard(){
        return board;
    }
    
    public boolean setRowCol(int row, int col) {
        this.row = row;
        this.col = col;
        if (board[row - 1][col - 1] == '-') {
            board[row - 1][col - 1] = currentPlayer.getSymbol();
            if(!checkWin() && !checkDraw()){
                switchPlayer();
            }
        } else {
            System.out.println("Move Failed!!");
            return false;
        }
        return true;
    }
    
    public boolean checkWin() {
        char symbol = currentPlayer.getSymbol();
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == symbol && board[i][1] == symbol && board[i][2] == symbol) {
                return true; // Row win
            }
            if (board[0][i] == symbol && board[1][i] == symbol && board[2][i] == symbol) {
                return true; // Column win
            }
        }
        if (board[0][0] == symbol && board[1][1] == symbol && board[2][2] == symbol) {
            return true; 
        }
        if (board[0][2] == symbol && board[1][1] == symbol && board[2][0] == symbol) {
            return true; 
        }
        
        return false;
    }
    
    public char getCurrentPlayer(){
        return currentPlayer.getSymbol();
    }

    public void switchPlayer() {
        if(currentPlayer == player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
    }
    
    public boolean checkDraw() {
        char[][] b = getBoard();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (b[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
}
