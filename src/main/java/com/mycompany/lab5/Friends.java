/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.util.HashSet;

/**
 *
 * @author boat5
 */
public class Friends {
    private int ID;
    private int age;
    private String name;
    private String tel;
    static private int Lastid = 1;
    
    Friends(String name,int age,String tel){
        this.name = name;
        this.ID = Lastid++;
        this.age = age;
        this.tel = tel;
    }

    public int getID() {
        return ID;
    }

    public int getAge() {
        return age;
    }

    public static int getLastid() {
        return Lastid;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public void setAge(int age) {
        if(age<=0) return;
        this.age = age;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Friend{" + " id= " + ID + " name= " + name + " tel= " + tel + " }";
    }

    public static void main(String[] args) {
        
    }
    
}
