/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 *
 * @author boat5
 */
public class TestReadFriend {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            File file = new File("Friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Friends f1 = (Friends) ois.readObject();
            Friends f2 = (Friends) ois.readObject();
            System.err.println(f1);
            System.err.println(f2);
            ois.close();
            fis.close();
        } catch (Exception e) {
            System.out.println("Error");
        }
    }
}
