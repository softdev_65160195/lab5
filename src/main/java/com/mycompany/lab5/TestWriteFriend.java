/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author boat5
 */
public class TestWriteFriend {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friends f1 = new Friends("Sakdinont Boonma",20,"0808088800");
            Friends f2 = new Friends("Prayut Janocha",65,"0919111911");
            System.err.println(f1);
            System.err.println(f2);
            File file = new File("Friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(f1);
            oos.writeObject(f2);
            oos.close();
            fos.close();
        } catch (Exception e) {
            System.err.println("Error");
        }
    }
}
